/* eslint-env browser */
(function() {
  'use strict';

  var isLocalhost = Boolean(window.location.hostname === 'localhost' ||
      window.location.hostname === '[::1]' ||
      window.location.hostname.match(
        /^127(?:\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}$/
      )
    );

  if ('serviceWorker' in navigator &&
      (window.location.protocol === 'https:' || isLocalhost)) {
    navigator.serviceWorker.register('service-worker.js')
    .then(function(registration) {
      // updatefound is fired if service-worker.js changes.
      registration.onupdatefound = function() {
        // updatefound is also fired the very first time the SW is installed,
        // and there's no need to prompt for a reload at that point.
        // So check here to see if the page is already controlled,
        // i.e. whether there's an existing service worker.
        if (navigator.serviceWorker.controller) {
          // The updatefound event implies that registration.installing is set:
          // https://slightlyoff.github.io/ServiceWorker/spec/service_worker/index.html#service-worker-container-updatefound-event
          var installingWorker = registration.installing;

          installingWorker.onstatechange = function() {
            switch (installingWorker.state) {
              case 'installed':
                // At this point, the old content will have been purged and the
                // fresh content will have been added to the cache.
                // It's the perfect time to display a "New content is
                // available; please refresh." message in the page's interface.
                break;

              case 'redundant':
                throw new Error('The installing ' +
                                'service worker became redundant.');

              default:
                // Ignore
            }
          };
        }
      };
    }).catch(function(e) {
      console.error('Error during service worker registration:', e);
    });
  }

  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showApp, error);
  } else {
    document.getElementByTagName('body').innerHTML = 'Geolocation is not supported by this browser.'
  }

  function error() {
    document.getElementById('app').style.display = 'none';
    document.querySelector('.loaderWrapper').style.display = 'none';
    document.getElementById('error').style.display = 'block';
  }

  function showApp(userLocation) {
    var lat,
        lng,
        appendeddatahtml,
        str,
        newstr,
        phone,
        rating,
        icon,
        address,
        distance,
        price,
        worktime,
        apiResponse = ''

    lat = userLocation.coords.latitude;
    lng = userLocation.coords.longitude;
    document.getElementById('app').style.display = 'block';
    getVenues();

    function today() {
      var today = new Date();
      var dd = today.getDate();
      var mm = today.getMonth()+1; //January is 0
      var yyyy = today.getFullYear();

      if (dd < 10) {
          dd='0'+dd;
      }

      if (mm < 10) {
          mm='0'+mm;
      }

      today = yyyy+dd+mm;

      return today;
    }

    function reqListener () {
      document.querySelector('.loaderWrapper').style.display = 'none';
    }

    var coffeeShopItems = []

    function getVenues() {
      var xhr = new XMLHttpRequest();
      xhr.addEventListener('load', reqListener)
      xhr.open('GET', "https://api.foursquare.com/v2/venues/explore/?ll="+lat+","+lng+"&venuePhotos=1&radius=5000&limit=10&client_id=WBA0WF2O3S01MQVAM1SWZGHLVFNLRHVDZZGDRR351Q3J35CY&client_secret=U4UQW4FCR2QC2XAHZXK3NXLRF3NCCWD5YOCVZDXO40ZZOJS4&v="+today()+"&query=coffee");
      xhr.onload = function() {
          if (xhr.status === 200) {
            var data = xhr.responseText;
            var jsonResponse = JSON.parse(data);
            document.querySelector('#venues').style.display = 'block';
            var dataArray = jsonResponse.response.groups[0].items;
            document.querySelector('#venues').innerHTML = '';
            
            var myOptions = {
              zoom: 13,
              center: new google.maps.LatLng(lat,lng-.03),
              mapTypeId: google.maps.MapTypeId.ROADMAP,
              panControl: false
            },
            map = new google.maps.Map(document.querySelector('#map'), myOptions);

            for (var i = 0; i < 10; i++){
              coffeeShopItems.push(dataArray[i]);
              var coffeeShop = dataArray[i].venue;


              // if (coffeeShop.categories[0]) {
              //   str = coffeeShop.categories[0].icon.prefix;
              //   newstr = str.substring(0, str.length - 1);
              //   icon = newstr+coffeeShop.categories[0].icon.suffix;
              // } else {
              //   icon = "";
              // }
              
              if (coffeeShop.contact.formattedPhone) {
                phone = "Phone:"+coffeeShop.contact.formattedPhone;
              } else {
                phone = "";
              }
              
              if (coffeeShop.location.address) {
                address = '<p class="subinfo">'+coffeeShop.location.address+'<br>';
              } else {
                address = "";
              }
              
              if (coffeeShop.rating) {
                rating = '<span class="rating">'+coffeeShop.rating+'</span>';
              }

              if (coffeeShop.location.distance) {
                distance = coffeeShop.location.distance
              }

              if (coffeeShop.price) {
                price = coffeeShop.price.messages;
              }

              
              appendeddatahtml = '<div class=" venue "><h2><span>'+coffeeShop.name+' '+rating+'</span></h2>'+address+phone+'</p><p><strong>Total Checkins:</strong> '+coffeeShop.stats.checkinsCount+'</p><p>Distance: '+distance+'</p><p>Price: '+price+'</p></div>';
              document.querySelector('#venues').innerHTML += appendeddatahtml;
              
              // Build markers
              var markerImage = {
                url: './images/pin2.png',
                scaledSize: new google.maps.Size(24, 24),
                origin: new google.maps.Point(0,0),
                anchor: new google.maps.Point(24/2, 24)
              },
              markerOptions = {
                map: map,
                position: new google.maps.LatLng(coffeeShop.location.lat, coffeeShop.location.lng),
                title: coffeeShop.name,
                animation: google.maps.Animation.DROP,
                icon: markerImage,
                optimized: false
              },
              marker = new google.maps.Marker(markerOptions)
            }
            sortResults(1);
          }
          else {
              alert('Request failed.  Returned status of ' + xhr.status);
          }
      };
      xhr.send();
    }

    document.getElementById('distance').addEventListener("click", function(){
      sortResults(1);
    });

    document.getElementById('price').addEventListener("click", function(){
      sortResults();
    });

    function sortResults(distance, price) {
      var dataEval = eval(coffeeShopItems);

      if (distance) {
        dataEval.sort(function(a,b){

          if(a.venue.location.distance == b.venue.location.distance)
            return 0;
          if(a.venue.location.distance < b.venue.location.distance)
            return -1;
          if(a.venue.location.distance > b.venue.location.distance)
            return 1;
        });
      } else {
        dataEval.sort(function(a,b){
          if(a.venue.price == null)
            return 1;
          if(b.venue.price == null)
            return 1;
          if(a.venue.price.tier == b.venue.price.tier)
            return 0;
          if(a.venue.price.tier < b.venue.price.tier)
            return -1;
          if(a.venue.price.tier > b.venue.price.tier)
            return 1;
        });
      }
      
      // for (var i = 0; i < coffeeShopItems.length; i++) {
      //   console.log(coffeeShopItems[i].venue.location.distance)
      // }
      document.querySelector('#venues').innerHTML = '';

      for (var i = 0; i < coffeeShopItems.length; i++){
        var coffeeShop = coffeeShopItems[i].venue

        // if (coffeeShop.categories[0]) {
        //   str = coffeeShop.categories[0].icon.prefix;
        //   newstr = str.substring(0, str.length - 1);
        //   icon = newstr+coffeeShop.categories[0].icon.suffix;
        // } else {
        //   icon = "";
        // }
        if(!coffeeShop.hours.isOpen) {
          continue;
        }
        if (coffeeShop.contact.formattedPhone) {
          phone = "Phone:"+coffeeShop.contact.formattedPhone;
        } else {
          phone = "";
        }
        
        if (coffeeShop.location.address) {
          address = '<p class="subinfo">'+coffeeShop.location.address+'<br>';
        } else {
          address = "";
        }
        
        if (coffeeShop.rating) {
          rating = '<span class="rating">'+coffeeShop.rating+'</span>';
        }

        if (coffeeShop.location.distance) {
          distance = coffeeShop.location.distance
        }

        if (coffeeShop.price) {
          price = coffeeShop.price.message;
        }
        
        appendeddatahtml = '<div class="venue"><h2><span>'+coffeeShop.name+' '+rating+'</span></h2>'+address+phone+'</p><p><strong>Total Checkins:</strong> '+coffeeShop.stats.checkinsCount+'</p><p>Distance: '+distance+'</p><p>Price: '+price+'</p></div>';
        document.querySelector('#venues').innerHTML += appendeddatahtml;
      }
    }

    function compareNumbers(a, b) {
      return a - b;
    }

    function mapbuild() {
      document.querySelector('#venues').style.display = 'none';
      var myOptions = {
      zoom:7,
      center: new google.maps.LatLng(44.0165,21.0059),
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      panControl: false
      },
      map = new google.maps.Map(document.querySelector('#map'), myOptions);
    }

    mapbuild();
  };
})();
